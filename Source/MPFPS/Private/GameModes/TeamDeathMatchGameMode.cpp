// Fill out your copyright notice in the Description page of Project Settings.


#include "GameModes/TeamDeathMatchGameMode.h"
#include "GameComponents/HealthComponent.h"

ATeamDeathMatchGameMode::ATeamDeathMatchGameMode() 
{
	//GameStateClass = ASGameState::StaticClass();
	//PlayerStateClass = ASPlayerState::StaticClass();

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickInterval = 1.0f;
}

void ATeamDeathMatchGameMode::StartPlay()
{
}

void ATeamDeathMatchGameMode::Tick(float DeltaSeconds)
{
}

void ATeamDeathMatchGameMode::CheckAnyPlayerAlive()
{
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		APlayerController* PC = It->Get();
		if (PC && PC->GetPawn())
		{
			APawn* MyPawn = PC->GetPawn();
			UHealthComponent* HealthComp = Cast<UHealthComponent>(MyPawn->GetComponentByClass(UHealthComponent::StaticClass()));
			if (ensure(HealthComp) && HealthComp->GetHealth() > 0.0f)
			{
				// A player is still alive.
				return;
			}
		}
	}

	// No player alive
	GameOver();
}

void ATeamDeathMatchGameMode::GameOver()
{
}

void ATeamDeathMatchGameMode::RestartDeadPlayers()
{
}
