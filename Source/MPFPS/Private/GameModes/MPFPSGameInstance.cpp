// Fill out your copyright notice in the Description page of Project Settings.


#include "GameModes/MPFPSGameInstance.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"

UMPFPSGameInstance::UMPFPSGameInstance(const FObjectInitializer& ObjectInitializer)
{
	UE_LOG(LogTemp, Warning, TEXT("GameInstance Constructor"));
	static ConstructorHelpers::FClassFinder<UUserWidget> MainMenuBPClass(TEXT("/Game/Blueprints/UI/Widgets/Menus/W_MainMenu"));
	if (!ensure(MainMenuBPClass.Class != nullptr)) return;
	MenuClass = MainMenuBPClass.Class;

}

void UMPFPSGameInstance::Init()
{
	UE_LOG(LogTemp, Warning, TEXT("Found Class %s"), *MenuClass->GetName());
}

void UMPFPSGameInstance::LoadMainMenu()
{
	if (!ensure(MenuClass != nullptr)) return;
	UUserWidget* MainMenu = CreateWidget<UUserWidget>(this, MenuClass);
	if (!ensure(MainMenu != nullptr)) return;
	MainMenu->bIsFocusable = true;
	MainMenu->AddToViewport();

	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;
	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(MainMenu->TakeWidget()); // Lecture Changing UI Input Modes 6:55
}
