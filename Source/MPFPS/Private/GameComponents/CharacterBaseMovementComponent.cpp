// Fill out your copyright notice in the Description page of Project Settings.


#include "GameComponents/CharacterBaseMovementComponent.h"
#include "GameCharacter/CharacterBase.h"
#include "GameFramework/CharacterMovementComponent.h"
//----------------------------------------------------------------------//
// UPawnMovementComponent
//----------------------------------------------------------------------//
UCharacterBaseMovementComponent::UCharacterBaseMovementComponent(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}
float UCharacterBaseMovementComponent::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	const ACharacterBase* CharacterBaseOwner = Cast<ACharacterBase>(PawnOwner);
	if (CharacterBaseOwner)
	{
		if (CharacterBaseOwner->IsAimDownSight())
		{
			MaxSpeed *= CharacterBaseOwner->GetAimDownSightSpeedModifier();
		}
		//if (CharacterBaseOwner->IsRunning())
		//{
		//	MaxSpeed *= CharacterBaseOwner->GetRunningSpeedModifier();
		//}
	}

	return MaxSpeed;
}
