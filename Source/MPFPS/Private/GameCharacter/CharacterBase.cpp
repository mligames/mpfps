// Fill out your copyright notice in the Description page of Project Settings.


#include "GameCharacter/CharacterBase.h"
#include "MPFPS/MPFPS.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "GameComponents/CharacterBaseMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameWeapon/WeaponBase.h"
#include "GameComponents/HealthComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ACharacterBase::ACharacterBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UCharacterBaseMovementComponent>(ACharacter::CharacterMovementComponentName))
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
	SpringArmComp->bUsePawnControlRotation = true;
	SpringArmComp->SetupAttachment(RootComponent);

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetupAttachment(SpringArmComp);

	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	GetMesh()->SetCollisionObjectType(ECC_Pawn);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	//GetMesh()->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);

	/*AimDownSightSpeedModifier = 0.5f;*/
	HealthComp = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComp"));
}
void ACharacterBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	CharacterBaseMovementComp = Cast<UCharacterBaseMovementComponent>(Super::GetMovementComponent());
}
// Called when the game starts or when spawned
void ACharacterBase::BeginPlay()
{
	Super::BeginPlay();

	GetHealthComp()->OnHealthChanged.AddDynamic(this, &ACharacterBase::OnHealthChanged);

	if (HasAuthority())
	{
		// Spawn a default weapon
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		CurrentWeapon = GetWorld()->SpawnActor<AWeaponBase>(StarterWeaponClass, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
		if (CurrentWeapon)
		{
			CurrentWeapon->SetOwner(this);
			CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponAttachPoint);
			UE_LOG(LogTemp, Warning, TEXT("CurrentWeapon Successfully Created..."));
		}
	}
}

// Called every frame
void ACharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &ACharacterBase::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACharacterBase::MoveRight);

	PlayerInputComponent->BindAxis("LookUp", this, &ACharacterBase::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookRight", this, &ACharacterBase::AddControllerYawInput);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ACharacterBase::StartCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ACharacterBase::EndCrouch);

	PlayerInputComponent->BindAction("AimDownSight", IE_Pressed, this, &ACharacterBase::StartAimDownSight);
	PlayerInputComponent->BindAction("AimDownSight", IE_Released, this, &ACharacterBase::StopAimDownSight);

	PlayerInputComponent->BindAction("FireEquipped", IE_Pressed, this, &ACharacterBase::StartFire);
	PlayerInputComponent->BindAction("FireEquipped", IE_Released, this, &ACharacterBase::StopFire);
}



FVector ACharacterBase::GetPawnViewLocation() const
{
	if (CameraComp)
	{
		return CameraComp->GetComponentLocation();
	}

	return Super::GetPawnViewLocation();
}

FName ACharacterBase::GetWeaponAttachPoint() const
{
	return WeaponAttachPoint;
}

AWeaponBase* ACharacterBase::GetCurrentWeapon() const
{
	return CurrentWeapon;
}

FRotator ACharacterBase::GetAimOffsets() const
{
	const FVector AimDirWorldSpace = GetBaseAimRotation().Vector();
	const FVector AimDirLocalSpace = ActorToWorld().InverseTransformVectorNoScale(AimDirWorldSpace);
	const FRotator AimRotLocalSpace = AimDirLocalSpace.Rotation();

	return AimRotLocalSpace;
}
void ACharacterBase::MoveForward(float Value)
{
	AddMovementInput(GetActorForwardVector() * Value);
}

void ACharacterBase::MoveRight(float Value)
{
	AddMovementInput(GetActorRightVector() * Value);
}
/*Crouch*/
void ACharacterBase::StartCrouch()
{
	Crouch();
}

void ACharacterBase::EndCrouch()
{
	UnCrouch();
}

/*Walking*/
bool ACharacterBase::IsWalking() const
{
	return bIsWalking;
}

void ACharacterBase::StartWalk()
{
	SetWalkState(true);
}

void ACharacterBase::EndWalk()
{
	SetWalkState(false);
}

void ACharacterBase::SetWalkState(bool bNewWalkState)
{
	bIsWalking = bNewWalkState;

	if (GetLocalRole() < ROLE_Authority)
	{
		ServerSetWalkState(bNewWalkState);
	}
}
bool ACharacterBase::ServerSetWalkState_Validate(bool bNewWalkState)
{
	return true;
}

void ACharacterBase::ServerSetWalkState_Implementation(bool bNewWalkState)
{
	SetWalkState(bNewWalkState);
}

/*AimDownSight*/
void ACharacterBase::StartAimDownSight()
{
	SetAimDownSight(true);
}

void ACharacterBase::StopAimDownSight()
{
	SetAimDownSight(false);
}

void ACharacterBase::SetAimDownSight(bool bNewADS)
{
	bIsAimDownSight = bNewADS;
	GEngine->AddOnScreenDebugMessage(0, 1.0f, FColor::Orange, FString::Printf(TEXT("SetAimDownSight Called")));
	if (GetLocalRole() < ROLE_Authority)
	{
		ServerSetAimDownSight(bNewADS);
	}
}

bool ACharacterBase::ServerSetAimDownSight_Validate(bool bNewADS)
{
	return true;
}

void ACharacterBase::ServerSetAimDownSight_Implementation(bool bNewADS)
{
	SetAimDownSight(bNewADS);
}

float ACharacterBase::GetAimDownSightSpeedModifier() const
{
	return AimDownSightSpeedModifier;
}

bool ACharacterBase::IsAimDownSight() const
{
	return bIsAimDownSight;
}

void ACharacterBase::StartFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartFire();
	}
	else 
	{
		UE_LOG(LogTemp, Warning, TEXT("ACharacterBase::StartFire() FAILED"));
	}
}

void ACharacterBase::StopFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ACharacterBase::StopFire() FAILED"));
	}
}

void ACharacterBase::OnHealthChanged(UHealthComponent* OwningHealthComp, float Health, float HealthDelta, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (Health <= 0.0f && !bDied)
	{
		// Die!
		bDied = true;

		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		DetachFromControllerPendingDestroy();

		SetLifeSpan(10.0f);
	}
}

UHealthComponent* ACharacterBase::GetHealthComp()
{
	return HealthComp;
}

void ACharacterBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACharacterBase, CurrentWeapon);
	DOREPLIFETIME(ACharacterBase, bDied);
	DOREPLIFETIME_CONDITION(ACharacterBase, bIsWalking, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(ACharacterBase, bIsAimDownSight, COND_SkipOwner);
}


