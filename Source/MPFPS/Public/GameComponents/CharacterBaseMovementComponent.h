// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "CharacterBaseMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class MPFPS_API UCharacterBaseMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
	//GENERATED_UCLASS_BODY()
public:
	UCharacterBaseMovementComponent(const FObjectInitializer& ObjectInitializer);

	virtual float GetMaxSpeed() const override;

};
