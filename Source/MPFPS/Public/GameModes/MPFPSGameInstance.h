// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MPFPSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MPFPS_API UMPFPSGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UMPFPSGameInstance(const FObjectInitializer& ObjectInitializer);

	virtual void Init();

	UFUNCTION(BlueprintCallable, Category = "Components")
	void LoadMainMenu();

private:
	TSubclassOf<class UUserWidget> MenuClass;
};
