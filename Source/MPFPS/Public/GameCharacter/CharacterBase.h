// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CharacterBase.generated.h"

class UCameraComponent;
class USpringArmComponent;
class UCharacterBaseMovementComponent;
class UHealthComponent;
class AWeaponBase;

UCLASS(ABSTRACT)
class MPFPS_API ACharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacterBase(const FObjectInitializer& ObjectInitializer);

/* Character */
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	/** Aim Offset Networking */
	/** get aim offsets */
	UFUNCTION(BlueprintCallable, Category = "Player|Aim")
	FRotator GetAimOffsets() const;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void PostInitializeComponents() override;

	virtual FVector GetPawnViewLocation() const override;

public:	
/* Camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USpringArmComponent* SpringArmComp;

/*************************************************************************************************************************************************/
/* Mobility                                                                                                                                      */
/*************************************************************************************************************************************************/
private:

	UCharacterBaseMovementComponent* CharacterBaseMovementComp;

public:

	UFUNCTION(BlueprintCallable, Category = "Components")
	FORCEINLINE class UCharacterBaseMovementComponent* GetCharacterBaseMovementComponent() const { return CharacterBaseMovementComp; }

protected:

	float MoveSpeed;

	float MoveDirection;

	UPROPERTY(Transient, Replicated)
	float TurnAxisValue;

	virtual void MoveForward(float Value);

	virtual void MoveRight(float Value);

protected:

	virtual void StartCrouch();

	virtual void EndCrouch();

/* Movement Walking */
protected:
	/** Current Walking State */
	UPROPERTY(Transient, Replicated)
	bool bIsWalking;

	virtual void StartWalk();

	virtual void EndWalk();

	/** [Server + Local] Change Walk State */
	void SetWalkState(bool bNewWalkState);

	/** [Server] Update Walking State */
	UFUNCTION(server, reliable, WithValidation)
	void ServerSetWalkState(bool bNewWalkState);

public:
	UFUNCTION(BlueprintCallable, Category = "Player|Movement")
	bool IsWalking() const;

/* Movement AimDownSight */
protected:

	UPROPERTY(EditDefaultsOnly, Category = "Player|Movement", meta = (ClampMin = 0.1, ClampMax = 100))
	float AimDownSightInterpSpeed;

	/** Current AimDownSight State */
	UPROPERTY(Transient, Replicated)
	bool bIsAimDownSight;

	/** Player Pressed AimDownSight Action */
	void StartAimDownSight();

	/** Player Released AimDownSight Action */
	void StopAimDownSight();

	/** [Server + Local] AimDownSight State */
	void SetAimDownSight(bool bNewADS);

	/** update targeting state */
	UFUNCTION(server, reliable, WithValidation)
	void ServerSetAimDownSight(bool bNewADS);

public:
	UPROPERTY(EditDefaultsOnly, Category = "Player|Movement")
	float AimDownSightSpeedModifier;
	/** get weapon taget modifier speed	*/
	UFUNCTION(BlueprintCallable, Category = "Player|Movement")
	float GetAimDownSightSpeedModifier() const;

	UFUNCTION(BlueprintCallable, Category = "Player|Movement")
	bool IsAimDownSight() const;

	//// Sprinting
	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
	//bool bIsSprinting;

	//UFUNCTION(BlueprintCallable, Category = "Movement")
	//virtual void StartSprint();

	//UFUNCTION(BlueprintCallable, Category = "Movement")
	//virtual void EndSprint();

	//UFUNCTION(BlueprintCallable, Category = "Movement")
	//bool IsSprinting() const;

/*************************************************************************************************************************************************/
/* Inventory / Weapon                                                                                                                            */
/*************************************************************************************************************************************************/
protected:
	/** socket or bone name for attaching weapon mesh */
	UPROPERTY(EditDefaultsOnly, Category = Weapon)
	FName WeaponAttachPoint;

	UPROPERTY(EditDefaultsOnly, Category = Weapon)
	TSubclassOf<AWeaponBase> StarterWeaponClass;

	UPROPERTY(Replicated)
	AWeaponBase* CurrentWeapon;

	/** get currently equipped weapon */
	UFUNCTION(BlueprintCallable, Category = Weapon)
	AWeaponBase* GetCurrentWeapon() const;

public:
	FName GetWeaponAttachPoint() const;

	UFUNCTION(BlueprintCallable, Category = Weapon)
	void StartFire();

	UFUNCTION(BlueprintCallable, Category = Weapon)
	void StopFire();

/*************************************************************************************************************************************************/
/* Health                                                                                                                                        */
/*************************************************************************************************************************************************/
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components|Health")
	UHealthComponent* HealthComp;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Player|Health")
	bool bDied;

	UFUNCTION()
	void OnHealthChanged(UHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

public:
	UFUNCTION(BlueprintCallable, Category = "Components|Health")
	UHealthComponent* GetHealthComp();
};