// Copyright Epic Games, Inc. All Rights Reserved.

#include "MPFPS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MPFPS, "MPFPS" );
