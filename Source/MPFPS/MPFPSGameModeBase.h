// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MPFPSGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MPFPS_API AMPFPSGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
